import "./src/config/dotenv_config.js";

import { gatherGlassdoorUserData } from "./src/handlers/glassdoor/glassdoor_handler.js";

const main = async () => {
  await gatherGlassdoorUserData();
};

main();
