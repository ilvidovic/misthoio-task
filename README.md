# Mistho.io task

## Quick Start Guide

Note: This guide assumes you have Git, Node.js and a UNIX-like shell installed.

Clone this repository.

    $ git clone git@bitbucket.org:ilvidovic/misthoio-task.git

Install dependencies.

    $ npm install

Initialize configuration.

    $ cp .env.example .env

Note: In order to successfully start this backend, it is required to configure it properly. This includes (but isn't limited to) replacing placeholders in configuration file `.env` with real values.

Run the server.

    $ npm run start
