import puppeteer from "puppeteer";

import { glassdoorLogin } from "./glassdoor_login.js";
import { glassdoorParseProfile } from "./glassdoor_parseProfile.js";
import { glassdoorDownloadResume } from "./glassdoor_downloadResume.js";
import { glassdoorLogout } from "./glassdoor_logout.js";

import { USERNAME, PASSWORD } from "../../util/constants.js";

const badusername = "example@example.com";
const badpassword = "examplepassword";

export const gatherGlassdoorUserData = async () => {
  const browser = await puppeteer.launch({ headless: true });
  // const browser = await puppeteer.launch({ headless: false });

  const page = await browser.newPage();

  // log into the service
  await glassdoorLogin({
    page,
    credentials: {
      // username: badusername,
      // password: badpassword,
      username: USERNAME,
      password: PASSWORD,
    },
  });

  // go to profile page and gather data
  await glassdoorParseProfile({ page });

  // store resume pdf
  await glassdoorDownloadResume({
    page: page,
    domain: new URL(page.url()).origin,
  });

  //  logout
  await glassdoorLogout({ page });
};
