import puppeteer from "puppeteer";

/**
 *
 * @param {{page: puppeteer.Page, credentials: {username: string, password: string}}} args
 * @return void
 */
export const glassdoorLogin = async ({
  page,
  credentials: { username, password },
}) => {
  await page.goto("https://www.glassdoor.com", { waitUntil: "networkidle2" });

  const acceptCookiesSelector = "button#onetrust-accept-btn-handler";
  await page.waitForSelector(acceptCookiesSelector, { timeout: 3000 });
  await page.click(acceptCookiesSelector);

  // sign in button
  const signInButtonSelector =
    "header button.LockedHomeHeaderStyles__signInButton";
  await page.waitForSelector(signInButtonSelector, { timeout: 3000 });
  await page.click(signInButtonSelector);

  const inputUserEmailSelector = "input#modalUserEmail";
  await page.waitForSelector(inputUserEmailSelector, { timeout: 3000 });
  await page.type(inputUserEmailSelector, username, { delay: 30 });

  const inputUserPasswordSelector = "input#modalUserPassword";
  await page.waitForTimeout(100);
  await page.type(inputUserPasswordSelector, password, { delay: 30 });

  const submitLogInFormButtonSelector =
    'form[name="emailSignInForm"] button[type="Submit"]';
  await page.waitForTimeout(200);
  await page.click(submitLogInFormButtonSelector);

  // check for incorrect credentials
  const logInErrorContainerSelector =
    'form[name="emailSignInForm"] div.textAndIconContainer span.description';
  await page
    .waitForSelector(logInErrorContainerSelector, { timeout: 6000 })
    .then((element) => {
      console.log(
        "Incorrect credentails, store this in the database and process"
      );
      throw new Error("Invalid credentials");
      // element exists, meaning we got back an error, assuming it's credentials issue
      // NL: "De gebruikersnaam en het wachtwoord zijn ongeldig.  Probeer het opnieuw."
    })
    .catch((notFound) => {
      if (!(notFound instanceof Error)) {
        console.log("Successful login, continue");
      }

      // this element did now show up, meaning credentials are okay
    });
};
