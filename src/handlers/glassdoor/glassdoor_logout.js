import puppeteer from "puppeteer";

/**
 *
 * @param {{page: puppeteer.Page}} args
 * @return void
 */
export const glassdoorLogout = async ({ page }) => {
  // log out
  const hamburgerMenuSelector = "#HamburgerButton";
  await page.waitForSelector(hamburgerMenuSelector, { timeout: 6000 });
  await page.click(hamburgerMenuSelector);

  const logOutLinkSelector = 'header a[href="/logout.htm"]';
  await page.waitForSelector(logOutLinkSelector, { timeout: 2000 });
  await page.click(logOutLinkSelector);
};
