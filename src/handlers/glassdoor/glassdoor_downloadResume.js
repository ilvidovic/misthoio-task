import puppeteer from "puppeteer";
import fs from "fs";

/**
 *
 * @param {{page: puppeteer.Page, domain: string}} args
 * @return void
 */
export const glassdoorDownloadResume = async ({ page, domain }) => {
  /**
   * Apparently downloading pdfs with .goto() is not possible (yet/still)
   * https://github.com/puppeteer/puppeteer/issues/2794
   */
  //   await page
  //     .goto(new URL("/member/profile/resumePdf.htm", domain), {
  //       waitUntil: "networkidle2",
  //     })
  //     .catch((err) => {
  //       console.log("err", err);
  //     });

  /**
   * Solution from: https://github.com/puppeteer/puppeteer/issues/299#issuecomment-340199753
   *
   * Ideally there is (?) a solution utilizing streams, so we don't download locally to machine first, and then upload to s3 or elsewhere
   *    With streams this nodejs instance would just be a proxy from glassdoor/puppeteer to s3 or other storage.
   */
  await page.exposeFunction("writeABString", async (strbuf, targetFile) => {
    return new Promise((resolve, reject) => {
      // Convert the ArrayBuffer string back to an ArrayBufffer, which in turn is converted to a Buffer
      const buf = strToBuffer(strbuf);

      // Try saving the file
      fs.writeFile(targetFile, buf, (err, text) => {
        if (err) reject(err);
        else resolve(targetFile);
      });
    });

    function strToBuffer(str) {
      // Convert a UTF-8 String to an ArrayBuffer
      let buf = new ArrayBuffer(str.length); // 1 byte for each char
      let bufView = new Uint8Array(buf);

      for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
      }
      return Buffer.from(buf);
    }
  });

  await page.evaluate(async (domain) => {
    function arrayBufferToString(buffer) {
      // Convert an ArrayBuffer to an UTF-8 String
      let bufView = new Uint8Array(buffer);
      let length = bufView.length;
      let result = "";
      let addition = Math.pow(2, 8) - 1;

      for (var i = 0; i < length; i += addition) {
        if (i + addition > length) {
          addition = length - i;
        }
        result += String.fromCharCode.apply(
          null,
          bufView.subarray(i, i + addition)
        );
      }
      return result;
    }

    let geturl = new URL("/member/profile/resumePdf.htm", domain);

    return fetch(geturl, {
      credentials: "same-origin", // usefull when we are logged into a website and want to send cookies
      responseType: "arraybuffer", // get response as an ArrayBuffer
    })
      .then((response) => response.arrayBuffer())
      .then((arrayBuffer) => {
        var bufstring = arrayBufferToString(arrayBuffer);

        // TODO: use unique file name
        const filename = "./" + Date.now() + "-downloadtest.pdf";
        return window.writeABString(bufstring, filename);
      })
      .catch(function (error) {
        console.log("Request failed: ", error);
      });
  }, domain);

  // TODO: listen for responses to determine whether download finished instead of waiting 30s
  await page.waitForTimeout(30000);
};
