import puppeteer from "puppeteer";

/**
 *
 * @param {{page: puppeteer.Page}} args
 * @return void
 */
export const glassdoorParseProfile = async ({ page }) => {
  page.on("response", responseEventHandler({ page }));

  await page.goto(new URL("/member/profile/index.htm", page.url()), {
    waitUntil: "networkidle2",
  });

  page.off("response");
};

const responseEventHandler =
  ({ page }) =>
  async (event) => {
    try {
      /**
       * Glassdoor has API on same origin domain
       *
       * For a more generic approach this could/would be determined from flag or value set in database for each site
       *
       */
      if (
        new URL(event.request().url()).origin === new URL(page.url()).origin &&
        ["POST", "GET"].includes(event.request().method())
      ) {
        // this JSON holds relevant profile data, such as skills, education, certs, etc...
        console.log("response event", await event.json());
      }
    } catch (err) {
      // console.log("response not parsable by .json()");
    }
  };
